﻿using MeuAcerto.Selecao.KataGildedRose.Entities;

namespace MeuAcerto.Selecao.KataGildedRose.Interfaces.Categoria
{
    public interface ICategoria
    {
        void AtualizarItem(Item item);
    }
}
