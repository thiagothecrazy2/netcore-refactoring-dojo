﻿using System.Collections.Generic;
using MeuAcerto.Selecao.KataGildedRose.Entities;

namespace MeuAcerto.Selecao.KataGildedRose.Interfaces
{
    public interface IGildedRoseRepositorio
    {
        IList<Item> GetList();
    }
}
