﻿using System;
using System.Collections.Generic;
using System.Text;
using MeuAcerto.Selecao.KataGildedRose.Entities;

namespace MeuAcerto.Selecao.KataGildedRose.Interfaces
{
    public interface IGildedRose
    {
        void AtualizarQualidade();

        IList<Item> Itens { get; }
    }
}
