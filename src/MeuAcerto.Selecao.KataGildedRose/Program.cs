﻿using System;
using MeuAcerto.Selecao.KataGildedRose.Data;
using MeuAcerto.Selecao.KataGildedRose.Interfaces;
using MeuAcerto.Selecao.KataGildedRose.Services;
using Microsoft.Extensions.DependencyInjection;

namespace MeuAcerto.Selecao.KataGildedRose
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var serviceProvider = new ServiceCollection()
				.AddSingleton<IGildedRoseRepositorio, GildedRoseRepositorio>()
				.AddSingleton<IGildedRose, GildedRose>()
				.BuildServiceProvider();

			var app = serviceProvider.GetService<IGildedRose>();

			for (var i = 0; i < 31; i++)
			{
				Console.WriteLine("-------- dia " + i + " --------");
				Console.WriteLine("Nome, PrazoValidade, Qualidade");
				foreach (var item in app.Itens)
				{
					Console.WriteLine(item.Nome + ", " + item.PrazoValidade + ", " + item.Qualidade);
				}
				Console.WriteLine("");
				app.AtualizarQualidade();
			}
		}
	}
}
