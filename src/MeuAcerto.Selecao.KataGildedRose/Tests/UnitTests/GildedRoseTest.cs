﻿using System.Collections.Generic;
using MeuAcerto.Selecao.KataGildedRose.Entities;
using MeuAcerto.Selecao.KataGildedRose.Services;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests.UnitTests
{
    /// <summary>
    /// Teste Unitário
    /// </summary>
    public class GildedRoseTest
    {
        [Fact]
        public void AtualizarQualidade_CategoriaPadraoPrazoValidadePositivoQualidadePositivo_Decrementar()
        {
            // Arrange
            var items = new List<Item> { new Item { Nome = "foo", PrazoValidade = 10, Qualidade = 4 } };
            var app = new GildedRose(items);

            //Act
            app.AtualizarQualidade();

            // Assert
            Assert.Equal(9, app.Itens[0].PrazoValidade);
            Assert.Equal(3, app.Itens[0].Qualidade);
        }

        [Theory]
        [InlineData(10, 20, 9, 19)]
        [InlineData(0, 20, -1, 18)]
        [InlineData(-1, 20, -2, 18)]
        [InlineData(-1, 1, -2, 0)]
        [InlineData(-1, 0, -2, 0)]
        public void AtualizarQualidadeCategoriaPadrao(int prazoValidade, int qualidade, int prazoValidadeEsperado, int qualidadeEsperado)
        {
            // Arrange
            var items = new List<Item> { new Item { Nome = "foo", PrazoValidade = prazoValidade, Qualidade = qualidade } };
            var app = new GildedRose(items);

            //Act
            app.AtualizarQualidade();

            // Assert
            Assert.Equal(prazoValidadeEsperado, app.Itens[0].PrazoValidade);
            Assert.Equal(qualidadeEsperado, app.Itens[0].Qualidade);
        }

        [Theory]
        [InlineData(10, 20, 9, 18)]
        [InlineData(0, 20, -1, 16)]
        [InlineData(-1, 20, -2, 16)]
        [InlineData(-1, 3, -2, 0)]
        [InlineData(-1, 2, -2, 0)]
        [InlineData(-1, 1, -2, 0)]
        [InlineData(-1, 0, -2, 0)]
        public void AtualizarQualidadeCategoriaConjurado(int prazoValidade, int qualidade, int prazoValidadeEsperado, int qualidadeEsperado)
        {
            // Arrange
            var items = new List<Item> { new Item { Nome = "conjurado", PrazoValidade = prazoValidade, Qualidade = qualidade } };
            var app = new GildedRose(items);

            //Act
            app.AtualizarQualidade();

            // Assert
            Assert.Equal(prazoValidadeEsperado, app.Itens[0].PrazoValidade);
            Assert.Equal(qualidadeEsperado, app.Itens[0].Qualidade);
        }
    }
}
