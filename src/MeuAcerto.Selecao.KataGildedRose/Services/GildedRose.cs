﻿using System.Collections.Generic;
using MeuAcerto.Selecao.KataGildedRose.Data;
using MeuAcerto.Selecao.KataGildedRose.Entities;
using MeuAcerto.Selecao.KataGildedRose.Entities.Categoria;
using MeuAcerto.Selecao.KataGildedRose.Interfaces;
using MeuAcerto.Selecao.KataGildedRose.Uteis;

namespace MeuAcerto.Selecao.KataGildedRose.Services
{
    public class GildedRose : IGildedRose
    {
        private readonly IGildedRoseRepositorio _gildedRoseRepositorio;
        public readonly IList<Item> _itens;

        public GildedRose(IGildedRoseRepositorio gildedRoseRepositorio)
        {
            _gildedRoseRepositorio = gildedRoseRepositorio;
            _itens = _gildedRoseRepositorio.GetList();
        }

        public GildedRose(IList<Item> itens) 
        {
            _itens = itens;
        }

        public IList<Item> Itens
            => _itens;

        public void AtualizarQualidade()
        {
            foreach (var item in Itens)
            {
                AtualizarItem(item);
            }
        }

        private void AtualizarItem(Item item)
        {
            var itemNome = item.Nome.ToLowerInvariant();

            if (itemNome.StartsWith("queijo"))
            {
                Singleton<CategoriaValorizacao>.Instance.AtualizarItem(item);
            }
            else if (itemNome.StartsWith("ingressos"))
            {
                Singleton<CategoriaValorizacaoValidade>.Instance.AtualizarItem(item);
            }

            else if (itemNome.StartsWith("sulfuras"))
            {
                Singleton<CategoriaLendario>.Instance.AtualizarItem(item);
            }

            else if (itemNome.Contains("conjurado"))
            {
                Singleton<CategoriaConjurado>.Instance.AtualizarItem(item);
            }
            else
            {
                Singleton<CategoriaPadrao>.Instance.AtualizarItem(item);
            }
        }        
    }
}
