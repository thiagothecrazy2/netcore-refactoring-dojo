﻿namespace MeuAcerto.Selecao.KataGildedRose.Entities.Categoria
{
    public class CategoriaValorizacao : CategoriaPadrao
    {
        protected override void AtualizarQualidadeItem(Item item)
        {
            if (item.Qualidade < 50)
            {
                item.Qualidade++;
            }
        }
    }
}
