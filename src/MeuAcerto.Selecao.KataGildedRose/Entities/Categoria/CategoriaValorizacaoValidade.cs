﻿namespace MeuAcerto.Selecao.KataGildedRose.Entities.Categoria
{
    public class CategoriaValorizacaoValidade : CategoriaValorizacao
    {
        public override void AtualizarItem(Item item)
        {
            if (item.PrazoValidade <= 0)
            {
                item.Qualidade = 0;
            }
            else
            {
                AtualizarQualidadeItem(item);

                if (item.PrazoValidade <= 10)
                    AtualizarQualidadeItem(item);

                if (item.PrazoValidade <= 5)
                    AtualizarQualidadeItem(item);
            }

            item.PrazoValidade--;
        }
    }
}
