﻿namespace MeuAcerto.Selecao.KataGildedRose.Entities.Categoria
{
    public class CategoriaConjurado : CategoriaPadrao
    {
        protected override void AtualizarQualidadeItem(Item item)
        {
            if (item.Qualidade > 1)
            {
                item.Qualidade -= 2;
            }
            else if (item.Qualidade > 0)
            {
                item.Qualidade--;
            }
        }
    }
}
