﻿using MeuAcerto.Selecao.KataGildedRose.Interfaces.Categoria;

namespace MeuAcerto.Selecao.KataGildedRose.Entities.Categoria
{
    public class CategoriaLendario : ICategoria
    {
        public void AtualizarItem(Item item)
        {
            item.Qualidade = 80;
        }
    }
}
