﻿using MeuAcerto.Selecao.KataGildedRose.Interfaces.Categoria;

namespace MeuAcerto.Selecao.KataGildedRose.Entities.Categoria
{
    public class CategoriaPadrao : ICategoria
    {
        public virtual void AtualizarItem(Item item)
        {
            AtualizarQualidadeItem(item);

            if (item.PrazoValidade <= 0)
            {
                AtualizarQualidadeItem(item);
            }

            item.PrazoValidade--;
        }

        protected virtual void AtualizarQualidadeItem(Item item)
        {
            if (item.Qualidade > 0)
            {
                item.Qualidade--;
            }
        }
    }
}
