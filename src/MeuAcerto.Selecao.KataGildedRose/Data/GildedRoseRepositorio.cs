﻿using System;
using System.Collections.Generic;
using System.Text;
using MeuAcerto.Selecao.KataGildedRose.Entities;
using MeuAcerto.Selecao.KataGildedRose.Interfaces;

namespace MeuAcerto.Selecao.KataGildedRose.Data
{
    public class GildedRoseRepositorio : IGildedRoseRepositorio
	{
        public IList<Item> GetList()
        {
			return new List<Item>{
				new Item {Nome = "Corselete +5 DEX", PrazoValidade = 10, Qualidade = 20},
				new Item {Nome = "Queijo Brie Envelhecido", PrazoValidade = 2, Qualidade = 0},
				new Item {Nome = "Elixir do Mangusto", PrazoValidade = 5, Qualidade = 7},
				new Item {Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = 0, Qualidade = 80},
				new Item {Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = -1, Qualidade = 80},
				new Item
				{
					Nome = "Ingressos para o concerto do TAFKAL80ETC",
					PrazoValidade = 15,
					Qualidade = 20
				},
				new Item
				{
					Nome = "Ingressos para o concerto do TAFKAL80ETC",
					PrazoValidade = 10,
					Qualidade = 49
				},
				new Item
				{
					Nome = "Ingressos para o concerto do TAFKAL80ETC",
					PrazoValidade = 5,
					Qualidade = 49
				},
				// Este item conjurado ainda não funciona direto!
				new Item {Nome = "Bolo de Mana Conjurado", PrazoValidade = 3, Qualidade = 6}
			};
		}
	}
}
